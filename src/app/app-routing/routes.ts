import { Routes } from '@angular/router';

import { EmpresaComponent } from '../empresa/empresa.component';
import { TablePostComponent } from '../table-post/table-post.component';

export const routes: Routes = [
  { path: 'empresa',  component: EmpresaComponent },
  { path: 'post',     component:  TablePostComponent},
  { path: '', redirectTo: '/empresa', pathMatch: 'full' }
];