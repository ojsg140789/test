import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing/app-routing.module';
import {MatCardModule} from '@angular/material/card';
import {MatListModule} from '@angular/material/list';
import { FlexLayoutModule } from '@angular/flex-layout';
import {MatInputModule} from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import {MatIconModule} from '@angular/material/icon';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { baseURL } from './shared/baseurl';
import {MatTableModule} from '@angular/material/table';

//Componets
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent } from './shared/footer/footer.component';
import { EmpresaComponent } from './empresa/empresa.component';
import { TarjetaPresentacionComponent } from './tarjeta-presentacion/tarjeta-presentacion.component';

//Services
import { PersonaServiceService } from './services/persona-service.service';
import { FormularioPersonaComponent } from './formulario-persona/formulario-persona.component';
import { TablePostComponent } from './table-post/table-post.component';
import { InterceptorService } from './interceptors/interceptor.service';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    EmpresaComponent,
    TarjetaPresentacionComponent,
    FormularioPersonaComponent,
    TablePostComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    AppRoutingModule,
    MatCardModule,
    MatListModule,
    FlexLayoutModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    HttpClientModule,
    MatTableModule
  ],
  providers: [PersonaServiceService, 
              {provide: 'BaseURL', useValue: baseURL},
              {provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true}], 
  bootstrap: [AppComponent]
})
export class AppModule { }
