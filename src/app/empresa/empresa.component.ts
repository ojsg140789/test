import { Component, OnInit } from '@angular/core';
import { Person } from '../shared/person';
import { PersonaServiceService } from '../services/persona-service.service';

@Component({
  selector: 'app-empresa',
  templateUrl: './empresa.component.html',
  styleUrls: ['./empresa.component.css']
})
export class EmpresaComponent implements OnInit {

  persons: Person[] = [];

  constructor( private personaServiceService: PersonaServiceService ) { }

  ngOnInit(): void {
    this.personaServiceService.getPersonas().subscribe( persons => this.persons = persons);
  }

  addNewPerson(newPerson:Person){
    this.persons.push(newPerson);
  }

}
