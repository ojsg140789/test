import { Component, OnInit } from '@angular/core';
import { PostService } from '../services/post.service';
import { Post } from '../shared/post';
import { Comment } from '../shared/comment';

@Component({
  selector: 'app-table-post',
  templateUrl: './table-post.component.html',
  styleUrls: ['./table-post.component.css']
})
export class TablePostComponent implements OnInit {

  posts: Post[] | any;
  comments: Comment[] | any;


  constructor(private postService: PostService) { }
  displayedColumns: string[] = [ 'id', 'title', 'user', 'body'];
  displayedColumnsComments: string[] = [ 'id', 'name', 'postId', 'email', 'body'];
  dataSource: Post[] | any;
  dataSourceComments: Comment[] | any;

  ngOnInit(): void {
    this.postService.getList()
    .subscribe(
      posts => this.posts = posts,
      err => console.error(err),
      () => {
            this.dataSource = this.posts;
      });
  }
  verComentarios( post:Post){
    this.postService.getComments( post.id )
    .subscribe(
      comments => this.comments = comments,
      err => console.error(err),
      () => {
            this.dataSourceComments = this.comments
      });
  };
}
