import { Direccion } from './direction'

export class Person {
    'nombre': string;
    'edad': number;
    'frase': string;
    'direccion': Direccion;
}
