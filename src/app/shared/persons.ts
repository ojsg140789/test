import { Person } from './person'

export const PERSONS: Person[] = [
    {
        nombre: 'Omar Soto',
        edad: 32,
        frase: 'Las rosas son rosas',
        direccion: {
            id: 1,
            calle: 'Av primero',
            colonia: 'San Pedro',
            pais: 'México'
        }
    },
    {
        nombre: 'Eduardo Soto',
        edad: 55,
        frase: 'Hell yeah!',
        direccion: {
            id: 2,
            calle: 'Av primero',
            colonia: 'San Pedro',
            pais: 'México'
        }
    },
    {
        nombre: 'Carmen García',
        edad: 45,
        frase: 'Your soulmate',
        direccion: {
            id: 3,
            calle: 'Av primero',
            colonia: 'San Pedro',
            pais: 'México'
        }
    },
    {
        nombre: 'Omar Soto 2.0',
        edad: 32,
        frase: 'Las rosas son rosas',
        direccion: {
            id: 1,
            calle: 'Av primero',
            colonia: 'San Pedro',
            pais: 'México'
        }
    },
    {
        nombre: 'Eduardo Soto 2.0',
        edad: 55,
        frase: 'Hell yeah!',
        direccion: {
            id: 2,
            calle: 'Av primero',
            colonia: 'San Pedro',
            pais: 'México'
        }
    },
    {
        nombre: 'Carmen García 2.0',
        edad: 45,
        frase: 'Your soulmate',
        direccion: {
            id: 3,
            calle: 'Av primero',
            colonia: 'San Pedro',
            pais: 'México'
        }
    },
    {
        nombre: 'Omar Soto 3.0',
        edad: 32,
        frase: 'Las rosas son rosas',
        direccion: {
            id: 1,
            calle: 'Av primero',
            colonia: 'San Pedro',
            pais: 'México'
        }
    },
    {
        nombre: 'Eduardo Soto 3.0',
        edad: 55,
        frase: 'Hell yeah!',
        direccion: {
            id: 2,
            calle: 'Av primero',
            colonia: 'San Pedro',
            pais: 'México'
        }
    },
    {
        nombre: 'Carmen García 3.0',
        edad: 45,
        frase: 'Your soulmate',
        direccion: {
            id: 3,
            calle: 'Av primero',
            colonia: 'San Pedro',
            pais: 'México'
        }
    },
    {
        nombre: 'Omar Soto 4.0',
        edad: 32,
        frase: 'Las rosas son rosas',
        direccion: {
            id: 1,
            calle: 'Av primero',
            colonia: 'San Pedro',
            pais: 'México'
        }
    },
    {
        nombre: 'Eduardo Soto 4.0',
        edad: 55,
        frase: 'Hell yeah!',
        direccion: {
            id: 2,
            calle: 'Av primero',
            colonia: 'San Pedro',
            pais: 'México'
        }
    },
    {
        nombre: 'Carmen García 4.0',
        edad: 45,
        frase: 'Your soulmate',
        direccion: {
            id: 3,
            calle: 'Av primero',
            colonia: 'San Pedro',
            pais: 'México'
        }
    }

];