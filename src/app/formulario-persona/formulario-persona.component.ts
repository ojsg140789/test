import { Component, OnInit, Output, EventEmitter, ViewChild  } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Person } from '../shared/person';

@Component({
  selector: 'app-formulario-persona',
  templateUrl: './formulario-persona.component.html',
  styleUrls: ['./formulario-persona.component.css']
})
export class FormularioPersonaComponent implements OnInit {
  @ViewChild('fform') personFormDirective : any;
  @Output() newPerson = new EventEmitter<Person>();
  addPersonForm: FormGroup | any;
  person: Person | any;

  constructor( private fb: FormBuilder) {
    this.createForm();
  }

  ngOnInit(): void {
  }

  createForm(){
    this.addPersonForm = this.fb.group({
      nombre: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-ZñÑ ]*')])],
      edad: [0, Validators.compose([Validators.required, Validators.min(0)])],
      frase: ['', Validators.required],
      direccion: '',
    });
  }

  addPerson() {
    this.person = this.addPersonForm.value;
    this.newPerson.emit(this.person);
    this.addPersonForm.reset({
      nombre: '',
      edad: 0,
      frase: '',
      direccion:''
    });
    this.personFormDirective.resetForm();
  }

}
