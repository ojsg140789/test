import { Component, OnInit, Input } from '@angular/core';
import { Person } from '../shared/person';

@Component({
  selector: 'app-tarjeta-presentacion',
  templateUrl: './tarjeta-presentacion.component.html',
  styleUrls: ['./tarjeta-presentacion.component.css']
})
export class TarjetaPresentacionComponent implements OnInit {

  @Input() persona: Person[] | any;

  constructor() { }

  ngOnInit(): void {
  }
}
