import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { baseURL } from '../shared/baseurl';
import { Post } from '../shared/post';
import { Comment } from '../shared/comment';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http: HttpClient) { }

  getList(): Observable<Post[]> {
    return this.http.get<Post[]>(baseURL + 'posts');
  }

  getComments( id:number ): Observable<Comment[]> {
    return this.http.get<Comment[]>(baseURL + 'posts/' + id + '/comments');
  }
}
