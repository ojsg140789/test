import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Person } from '../shared/person';
import { PERSONS } from '../shared/persons';


@Injectable({
  providedIn: 'root'
})
export class PersonaServiceService {

  constructor() { }

  getPersonas(): Observable<Person[]> {
    return of(PERSONS);
  }
}
